void call(){
    echo "please call each method as utils.methodName()"
}

public String determineBitBucketRepoName(){
    return scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
}

public String determineBitBucketAppcode(){
    return scm.getUserRemoteConfigs()[0].getUrl().split('/')[-2].toUpperCase()
}

public boolean verifyMap(Map defaultMap, Map toVerifyMap){
    echo "verifying the input parameters: ${toVerifyMap} against accepted parameters: ${defaultMap}"
    isMapVerified = false
    def missingKeys = ""
    try{
        defaultMap.each{
            k, v -> if(!toVerifyMap.containsKey(k)){
                missingKeys = "${missingKeys} ${k}"
            }
            else if(toVerifyMap[k] == null){
                missingKeys = "${missingKeys} ${k}"
            }
            else{
                defaultMap[k] = toVerifyMap[k]
                isMapVerified = true
            }
        }
    }
    catch(e){
        error "Exception while verifying the provided map"
    }
    if(isMapVerified && missingKeys == ""){
        echo "All Mandatory parameters provided"
    }
    else{
        error "Missing mandatory parameters: ${missingKeys}"
    }
    return isMapVerified
}

public String getResponse(String url, String credentials){
    def json = [:]
    try{
        def result = httpRequest acceptType: 'APPLICATION_JSON',
                                 contentType: 'APPLICATION_JSON',
                                 authentication: credentials,
                                 url: url
        json = readJSON text: result.content
    }
    catch(e){
        error "Unable to get the response: ${e}"
    }
    return json
}

public void postRequestWithNoData(String url, String credentials){
    //echo "postRequest for ${postData.event}"
    def result = httpRequest acceptType : 'APPLICATION_JSON',
                            contentType: 'APPLICATION_JSON',
                            httpMode: 'POST',
                            authentication: credentials,
                            url: url
    echo "response: ${result.content}"
}

public String shortGitCommit(String commitID){
    try{
        if(verifySHA(commitID)){
            return commitID[0..6]
        }
        else{
            error "Invalid commit ID: ${commitID}"
        }
    }
    catch(e){
        error "unable to create short commit ID: ${e}"
    }
}

public boolean verifySHA(String commitID){
    if ( commitID ==~ /^[0-9a-f]{40}$/){
        return true
    }
    else{
        return false
    }
}

public String buildDuration(){
    int minutes = currentBuild.duration.intdiv(1000*60)
    int seconds = currentBuild.duration.mod(1000*60)/1000
    return "${minutes}:${seconds}secs"
}

public String tmpDir(){
    try{
        def dirName = UUID.randomUUID().toString()
        echo "creating temp dir ${dirName}"
        def tmpDir = sh(script: "mkdir ${dirName} && cd ${dirName} && pwd && cd ..", returnStdout: true).trim()
        return tmpDir
    }
    catch(e){
        error "unable to create temp dir, Error: ${e}"
    }
}

public Map getAppConfig(String env){
    echo "utils.getAppConfig called for env: ${env}"
    def appData = [:]
    try{
        appData = readYaml file: 'mas_configs/config.yaml'
        appData = appData.get(env, [:])
    }
    catch(e){
        error "error while reading appData: ${e}"
    }
    echo "appdata return: ${appData}"
    return appData
}
