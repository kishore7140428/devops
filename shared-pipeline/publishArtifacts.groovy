void call(){

}

public String push(Map nexusMap){
    defaultMap = [
        version: '',
        artifact: '',
        artifact_type: '',
        repoName: '',
        appCode: ''
    ]

    utils.verifyMap(defaultMap, nexusMap)
    def responseCode = {}
   
    String artifactVersion = nexusMap.version
  
    echo "artifact version: ${artifactVersion}" //remove
    String artifactName = nexusMap.repoName + '-' + artifactVersion
    String artifactURL = "http://${pipeLineGlobals.nexusURL()}/repository/${nexusMap.appCode}/${nexusMap.repoName}/${nexusMap.repoName}/${artifactVersion}/${artifactName}.${nexusMap.artifact_type}"
    def skipUpload = false
    try{
        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: pipeLineGlobals.nexusCredentials(), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
            if (isUnix()) {
                println("checking artifacts - unix")
                responseCode = sh (script: "curl -I -u ${USERNAME}:${PASSWORD} ${artifactURL} | head -n 1 | awk '{print \$2}'", returnStdout: true)
                println(responseCode)
            }
            else {
                println("checking artifacts - windows")
                responseCode = powershell(script:"""
                try
                {
                    (iwr -Uri \'${artifactURL}\' -Headers @{ Authorization = \'Basic \'+ [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes(\'${USERNAME}:${PASSWORD}\')) }).StatusCode
                } catch {
                    \$_.Exception.Response.StatusCode.value__
                }
                """, returnStdout: true)
                println(responseCode)
            }
            println("validate responsecode")
            if(responseCode.trim() == "200"){
                echo "Artifact already exists: ${artifactURL}"
            }
            else{
                echo "Uploading ${artifactURL}... "
                nexusArtifactUploader artifacts: [
                    [artifactId: nexusMap.repoName, classifier: '', file: nexusMap.artifact, type: nexusMap.artifact_type]],
                    credentialsId: pipeLineGlobals.nexusCredentials(),
                    groupId: nexusMap.repoName,
                    nexusUrl: pipeLineGlobals.nexusURL(),
                    nexusVersion: 'nexus3', 
                    protocol: 'http', 
                    repository: nexusMap.appCode, 
                    version: artifactVersion
            }
        }
        
    }
    catch(e){
        error "Exception while checking the artifact exists and upload: ${e}"
    }
    return artifactURL
}