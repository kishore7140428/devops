void call(){
    echo "Please call each method as updategitLab.methodName()s"
}

public void inProgress(Map glMap){
    validate(glMap)
    postTogitLab(pipeLineGlobals.gitLabInProgress(), glMap)
}

public void success(Map glMap){
    validate(glMap)
    postTogitLab(pipeLineGlobals.gitLabSuccess(), glMap)
}

public void failed(Map glMap){
    validate(glMap)
    postTogitLab(pipeLineGlobals.gitLabFailure(), glMap)
}

private void validate(Map glMap){
    Map mandatoryProp = ['commitID': '', 'url': '', 'message': '', 'name': '']
    utils.verifyMap(mandatoryProp, glMap)
}

private void postTogitLab(String status, Map glMap){
    String msgBody = """
        {
            "state": "${status}",
            "description": "${glMap.message}",
            "name": "${glMap.name}",
            "key": "${glMap.name}",
            "url": "${glMap.url}"
        }
    """
   echo "GitLab POST: ${msgBody}"

    httpRequest acceptType: 'APPLICATION_JSON',
                contentType: 'APPLICATION_JSON',
                httpMode: 'POST',
      			customHeaders: [[name:'Gitlabtoken', value:'glft-sSF3gGzoZ7EFCm2xoxz_']],
                //authentication: pipeLineGlobals.gitLabCreds(),
               //url: "${pipeLineGlobals.gitLabRestURL()}statuses/${glMap.commitID}"
              //url: "${pipeLineGlobals.gitLabRestURL()}/facimate.git"
                url: "https://gitlab.com/api/v4/projects/54419510/statuses/2f57eac3834bda64f21a6cabfe7fe47d14bbcc4c?name=Job1&state=pending&description=Pending&pipeline_id=-1"
}
