void call(){

}

static final String bitBucketInProgress(){
    return "INPROGRESS"
}
static final String sonarToken(){
    return "sonarqubetoken"
}

static final String gitLabRestURL(){
    return "https://gitlab.com/logicfocus.in/api/v4/"
}
static final String gitlabURL(){
    return "https://gitlab.com/logicfocus.in/"
}
static final String gitLabCreds(){
    return "facimatetoken"
}

static final String sonarURL(){
    return "http://192.168.1.12:9000"
}

static final sonarCreds(){
    return "sonar2"
}

static final String nexusiqURL(){
    return "http://192.168.1.30:8081"
}

static final nexusiqCreds(){
    return "nexus"
}

static final String nexusURL(){
    return "192.168.1.30:8081"
}

static final String nexusHTTPURL(){
    return "192.168.1.30:8081"
}
static final String nexusCredentials(){
    return "nexus"
}
static final String nexusDockerURLhttps(){
    return "https://nexus.mas.gov.sg:8444"
}

static final String nexusDockerURL(){
    return "nexus.mas.gov.sg:8444"
}
static final String bitBucketSuccess(){
    return "SUCCESSFUL"
}

static final String bitBucketFailure(){
    return "FAILED"
}

static final List getMandatoryPromotionStages(String application){
    List mandatoryStages = []
    switch(application){
        case 'dotNetVM':
            mandatoryStages = ['APPLICATION PIPELINE', 'NEXUS PUBLISH', 'SONAR SCAN', 'UNIT TESTS']
            break
        default:
            error "Unrecognised application: ${application}"
            break
    }
    return mandatoryStages
}

