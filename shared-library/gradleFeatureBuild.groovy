def call(Map buildMap) {
echo "Python Pipeline called with params: ${buildMap}"
  def JENKINS_NODE = buildMap.node
  static final String repoName = utils.determineBitBucketRepoName()
  static final String appCode = utils.determineBitBucketAppcode()

  Map bitBucketPost = [
        'name': 'APPLICATION PIPELINE',
        'url': "${JOB_URL}${BUILD_NUMBER}",
        'commitID': "",
        'appCode': appCode,
        'repoName': repoName
  ]
  pipeline{
    agent {
        node{
            label JENKINS_NODE  
        }
    }
     stages{
           stage('Checkout SCM'){
            steps{
                script{
                    gitInfo = checkout scm
                    bitBucketPost['commitID'] = gitInfo.GIT_COMMIT
                    bitBucketPost['message'] = "Application Pipeline started"
                    updateBitBucket.inProgress(bitBucketPost)
                  }
                }
           }
          stage('Build'){
           
             steps{
                  script{
                     sh "${tool name: 'Gradle 7.4.2', type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle --no-daemon clean build"
                  }
              }
          }
          stage('Scan '){
             tools {
               jdk 'java-17'
           }
              steps{
                  withSonarQubeEnv('Sonar_12') {
                    sh "${tool name: 'Gradle 7.4.2', type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle sonarqube"
                    
                    }
              }
          }
          stage('Run unit Test '){
               when {
                    expression { fileExists("src/test") }
                }
              steps{
                  script{
                     echo " Run  Unit Test"
                  }
              }
          }
         
    }
    post{
        success{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} successful in ${utils.buildDuration()}"
                updateBitBucket.success(bitBucketPost)
            }
        }
        unstable{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} successful in ${utils.buildDuration()}"
                updateBitBucket.success(bitBucketPost)
            }
        }
        failure{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} failed"
                updateBitBucket.failed(bitBucketPost)
            }
        }
    }
  }  
}
