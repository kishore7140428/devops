def call(Map relMaps) {
echo "Python Pipeline called with params: ${relMaps}"
  def JENKINS_NODE = relMaps.node

  pipeline{
    agent {
        node{
            label JENKINS_NODE 
        }
    }
     stages{
           stage('Checkout SCM'){
            steps{
                script{
                    gitInfo = checkout scm
                  }
                }
           }
          stage('Get Package'){
              steps{
                  script{
                      def group=relMaps.groupId.replaceAll("\\.","/")
                      echo "${group}"
                      sh "curl -O http://${pipeLineGlobals.nexusURL()}/repository/${relMaps.repo}/${group}/${relMaps.artifactId}/${relMaps.version}/${relMaps.artifactId}-${relMaps.version}.${relMaps.ext}"
                  }
              }
          }
          stage('Get scripts'){
              steps{
                  script{
                      echo " copy scripts to target dir"
                  }
              }
          }

          stage('create jar'){
              steps{
                  script{
                      sh "jar -r component.jar .  -x '*.git*' Jenkinsfile"
                  }
              }
          }

          stage('publish to nexus'){
              steps{
                  script{
                      nexusArtifactUploader artifacts: [[artifactId: 'QualityDemo',
                                                   classifier: '',
                                                   file: "component.jar",
                                                   type: 'jar']],
                        credentialsId: 'CICD',
                        groupId: 'com.logicfocus',
                        nexusUrl: "${pipeLineGlobals.nexusURL()}",
                        nexusVersion: 'nexus3',
                        protocol: 'http',
                        repository: 'OPTIMUM',
                        version: gitInfo.GIT_COMMIT
                  }
              }
          }
           stage('Generate Artifact'){
              steps{
                  script{
                    def checkScumTxt =httpRequest "http://${pipeLineGlobals.nexusURL()}/service/rest/v1/search/assets/download?repository=OPTIMUM&sort=version&maven.groupId=com.logicfocus&maven.artifactId=QualityDemo&maven.baseVersion=1.0.1&maven.extension=jar.sha1&maven.classifier="
                    sh "echo '{ \"groupId\":\"com.web.net\"' > artifact.txt"
                    sh "echo ',\"checksum\":\"${checkScumTxt.content}\"' >> artifact.txt"
                    sh "echo ',\"artifactId\":\"QualityDemo\"' >> artifact.txt"
                    sh "echo ',\"repoId\":\"OPTIMUM\"' >> artifact.txt"
                    sh "echo ',\"ext\":\"jar\"' >> artifact.txt"
                    sh "echo ',\"versionId\":\"${gitInfo.GIT_COMMIT}\" }' >> artifact.txt"

                  }
              }
          }
    }

    post {
        always {
            archiveArtifacts artifacts: 'artifact.txt', onlyIfSuccessful: true
        }
    }
  }  
}
