def call(Map buildMap) {
echo "Python Pipeline called with params: ${buildMap}"
  def JENKINS_NODE = buildMap.node
  static final String repoName = utils.determineBitBucketRepoName()
  static final String appCode = utils.determineBitBucketAppcode()
  def gradleName = "Gradle 7.4.2"
  def appName, appVersion
  def artifactType = "jar"
  Map bitBucketPost = [
        'name': 'APPLICATION PIPELINE',
        'url': "${JOB_URL}${BUILD_NUMBER}",
        'commitID': "",
        'appCode': appCode,
        'repoName': repoName
  ]
  pipeline{
    agent {
        node{
            label JENKINS_NODE 
        }
    }
     stages{
           stage('clean workspace'){
                steps{
                    deleteDir()
                }
            }
           stage('Checkout SCM'){
            steps{
                script{
                    gitInfo = checkout scm
                    bitBucketPost['commitID'] = gitInfo.GIT_COMMIT
                    bitBucketPost['message'] = "Application Pipeline started"
                    updateBitBucket.inProgress(bitBucketPost)
                  }
                }
           }
          stage('Build'){
             
              steps{
                  script{
                     sh "${tool name: gradleName, type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle --no-daemon clean build"
                  }
              }
          }
          stage('Scan '){
             tools {
               jdk 'java-17'
           }
              steps{
                  withSonarQubeEnv('Sonar_12') {
                    sh "${tool name: gradleName, type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle sonarqube"
                    
                    }
              }
          }
          stage('Run unit Test '){
               when {
                    expression { fileExists("src/test") }
                }
              steps{
                  script{
                     echo " Run  Unit Test"
                  }
              }
          }
           stage('Create Package '){
             
              steps{
                  script{
                     echo " Creating jar file"
                     appVersion = sh (script: "${tool name: gradleName, type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle --no-daemon properties | grep -v grep | grep version | cut -d ':' -f 2",returnStdout: true).trim()
                     appName = sh (script: "${tool name: gradleName, type: 'hudson.plugins.gradle.GradleInstallation'}/bin/gradle  --no-daemon properties | grep -v grep | grep name | cut -d ':' -f 2",returnStdout: true).trim()
                     echo "App version : ${appVersion}"
                     echo "App Name : ${appName}"
                     fileOperations([
                        folderCreateOperation('release'),
                        folderCopyOperation(sourceFolderPath: "deploy", destinationFolderPath: 'release/deploy/'),
                        fileCopyOperation(flattenFiles: true,includes: "build/libs/${appName}-${appVersion}.jar", targetLocation: 'release/')
                        ])
                      jar jarFile: "${appName}-${appVersion}.${artifactType}", archive: false, dir: 'release'
                  }
                
              }
          }
          stage('Publish to Nexus'){
    
                steps{
                    script{
                        sh "ls -ltr"
                        
                        nexusUploadFile = "${appName}-${appVersion}.${artifactType}"
                        //"${fileProps.packageId}.${fileProps.version}.${artifactType}"
                        echo "nexus file: ${nexusUploadFile}"
                        Map artifactMap = [
                            version: appVersion,
                            artifact: nexusUploadFile,
                            artifact_type: artifactType,
                            repoName: repoName,
                            appCode: appCode
                        ]
                        def artifactURL = publishArtifacts.push(artifactMap)
                        Map bitBucketMap = [
                        'name': 'NEXUS PUBLISH',
                        'commitID': gitInfo.GIT_COMMIT,
                        'appCode': appCode,
                        
                        'repoName': repoName,
                        'url': artifactURL,
                        'message': "Nexus publish is success"
                    ]
                    updateBitBucket.success(bitBucketMap)
                    }
                }
            }
            stage('Build Info') {
              steps {
                  script {
                      def checkSumTxt = sha1 "${appName}-${appVersion}.${artifactType}"

                      sh "echo '{ '  > buildData.txt "
                      sh "echo ' \"buildnumber\":\"${BUILD_NUMBER}\"' >> buildData.txt"
                      sh "echo ' ,\"buildurl\":\"${JOB_URL}\"' >> buildData.txt"
                      sh "echo ' ,\"group\":\"QualityDemo\"' >> buildData.txt"
                      sh "echo ' ,\"checksum\":\"${checkSumTxt}\"' >> buildData.txt"
                      sh "echo ' ,\"artifact\":\"${appName}\"' >> buildData.txt"
                      sh "echo ' ,\"ext\":\"${artifactType}\"' >> buildData.txt"
                      sh "echo ' ,\"version\":\"${appVersion}\" }' >> buildData.txt"
                      

                  }
              }
          }

         
    }
    post{
        success{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} successful in ${utils.buildDuration()}"
                updateBitBucket.success(bitBucketPost)
            }
        }
        unstable{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} successful in ${utils.buildDuration()}"
                updateBitBucket.success(bitBucketPost)
            }
        }
        failure{
            script{
                bitBucketPost.message = "build ${BUILD_NUMBER} failed"
                updateBitBucket.failed(bitBucketPost)
            }
        }
        always {
            archiveArtifacts artifacts: 'buildData.txt', onlyIfSuccessful: true
        }
    }
  }  
}

