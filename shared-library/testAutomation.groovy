def call(Map relMaps) {
	echo "Python Pipeline called with params: ${relMaps}"
  def JENKINS_NODE = relMaps.node

	def repoID = 'com.optimum.net'
	def testFrameGroup = 'com.optimum.net'
	def testFrameArtifact = 'testCore'
	def versionId = 'testCore'

  pipeline{
    agent {
        node{
            label JENKINS_NODE 
        }
    }
     stages{
     	stage('Setup Test framework'){
     		steps{

     		def group=testFrameGroup.replaceAll("\\.","/")
            echo "${group}"
            sh "curl -O http://${pipeLineGlobals.nexusURL()}/repository/${repoID}/${group}/${testFrameArtifact}/${versionId}/${testFrameArtifact}-${versionId}.zip"
                  
     		}
     	}
     	stage('Download test scripts'){
              steps{
                  script{
             //         def group=relMaps.groupId.replaceAll("\\.","/")
            echo "${group}"
           // sh "curl -O http://${pipeLineGlobals.nexusURL()}/repository/${relMaps.repo}/${group}/${relMaps.artifactId}/${relMaps.version}/${relMaps.artifactId}-${relMaps.version}.${relMaps.ext}"
             
                  }
              }
          }

     }

 }
}